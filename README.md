## Presentacin para webinar sobre infosec con ODIA

[Page](https://rlyehlab.gitlab.io/talleres-presentaciones/sec-org/webinar-odia)

[surce](https://pad.disroot.org/p/webinar-seguridad-digital)

## Material previo:

    Guía del hacklab:

    https://gitlab.com/rlyehlab/sec-org/sec-org-material

    Clonar y abrir index.html desde el navegador (y habilitar javascript).

    Otra guía orientada a organizaciones sociales:

    https://protejete.wordpress.com/gdr_principal/

    Biblioteca de recursos de seguridad de todo tipo:

    https://salama.io/ (bajo "security library")

    Repertorio de guías, por temática

    https://securityinabox.org/es/ (la versión en inglés tiene más cosas y está más actualizada)

    
# Ejes:

    Seguridad Individual/org (acciones individuales y grupales, la seguridad ES colectiva)

    espionaje privado y estatal, grandes influencias en la sociedad (elecciones, percepción de la realidad)

    Ciudadania digital

    Soberania informatica

    esto no es mano del estado, sino servidores propios, infraestructura descentralizada


# División de temas:
    - Sección organizacional/conceptual: Solar y Saico
    - Sección técnica: Jim, Nan y Saio
    Cada une de nosotres elige a su vez un tema en particular para hablar que le interese, enmarcado dentro de alguno de los 3 Ejes

## Público:

    Organizaciones sociales

    Organizaciones políticas


## Amenazas comunes del Público:

    Fotos en manifestaciones

    Acoso digital

    Doxxing

    Persecucion Politica

    Persecucion privada

    Robo de identidad

    Robo/destruccion de datos

    Pérdida de datos por fallos en los sistemas operativos/hardware

    Grupos públicos de Whatsapp en general

    Más amenazas en https://protejete.wordpress.com/gdr_principal/amenazas_vulnerabilidades/


## ¿Qué es la seguridad informática?

    https://protejete.wordpress.com/gdr_principal/definicion_si/


# Modelo de amenazas (según EFF https://ssd.eff.org/en/module/your-security-plan )

    Qué es lo que quiero proteger?

    De quién lo quiero proteger?

    Qué tan probable es que necesite protegerlo?

    Cuáles son las consecuencias si fallo en protegerlo?

    Cuánto estoy dispuesto a hacer para protegerlo?


# Conceptos claves:

    Comunicaciones seguras

    Medios de comunicación más comunes, a grandes razgos:

    Llamadas de voz por celular o teléfono de línea

    SMS

    Emails

    Aplicaciones de mensajería instantánea sobre internet (WiFi o red celular)

    Sitios online tipo foros o redes sociales

    Análisis de riesgos

    Seguridad como práctica continua

    Seguridad como práctica colectiva

    Puesta en valor de conocimientos

    Diferencia entre infraestructura estatal y distribuida

    Anonimato como derecho

    Privacidad vs anonimato

    Si la entrada es gratis vos sos el producto, ¿Que significa?


## Cosas que hemos aprendido de nuestra experiencia:
        * El mayor obstaculo de las organizaciones es la heterogeneidad de conocimientos y tecnologías
        * Sentirse seguro por usar una herramienta es peor que saber que te estan vigilando
        * El grueso de los ataques son físicos (robo/destruccion de dispositivos)
        * osint es más común/accesible que otras tecnologías
        * Las tecnologías cambian y hay que mantenerse actualizado en lo posible


## License

![CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/88x31.png)  
All work is under *[Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es)*
